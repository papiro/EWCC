angular.module('TickReg', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider)
    {
        $routeProvider.
            when('/', {
                controller: 'DashboardController',
                templateUrl: 'dashboard.html'
            }).
            when('/Calculator', {
                controller: 'CalculatorController',
                templateUrl: 'Calculator.html'
            }).
            when('/DB', {
                controller: 'DBController',
                templateUrl: 'DB.html'
            }).
            otherwise('/error.html');
    }])
    .constant('Base',
    {
        ppA: 13.95,
        ppC: 6.95,
        ppD: 2.50,
        discountPercent: 0.2,
        gratuityPercent: 0.17,
        taxPercent: 0.06
    })
    .value('Ticket',
    {
        Server: undefined,
        Adults: 0,
        Children: 0,
        Drinks: 0,
        Discount: "no",
        Subtotal: 0,
        Total: 0
    })
    .service('Calculate', ['Base', 'Ticket', function(Base, Ticket) {

        this.Total = function() {
            var subtotal = Base.ppA * Ticket.Adults;
            subtotal += Base.ppC * Ticket.Children;
            subtotal += Base.ppD * Ticket.Drinks;

            var DiscountAMT = 0;
            if (Ticket.Discount == "yes") {
                DiscountAMT = subtotal * Base.discountPercent;
            }

            var GratuityAMT = 0;
            if (parseInt(Ticket.Adults) + parseInt(Ticket.Children) > 5) {
                GratuityAMT = subtotal * Base.gratuityPercent;
            }

            var Tax = (subtotal - DiscountAMT) * Base.taxPercent;

            //TOTAL
            Ticket.Total = subtotal - DiscountAMT - GratuityAMT + Tax;
            Ticket.Subtotal = subtotal;
            
            var amts = {
                    DiscountAMT: DiscountAMT,
                    GratuityAMT: GratuityAMT,
                    Tax: Tax
                };

            return amts;                
        };
    }])
    .controller("CalculatorController", ['$scope', 'Ticket', 'Calculate', 'Local', function($scope, Ticket, Calculate, Local)
        {
            $scope.guests = Ticket;

            $scope.amts = {
                DiscountAMT: 0,
                GratuityAMT: 0,
                Tax: 0
            };

            //running Calculate.Total updates 'guests' with calculations
            //return value of Calculate.Total is amts object with results from Calculate service
            $scope.$watchCollection('guests', function(){
                $scope.amts = Calculate.Total();
            });

            $scope.test = Local.test;
            $scope.deleteAll = Local.delete.all;
            $scope.save = function(){
                var guests = $scope.guests;

                if(guests.Adults && guests.Server){
                    Local.save();
                }                
            }
            $scope.retrieve = Local.retrieve.all;
        }])
    .service('Local', ['Ticket', '$rootScope', '$compile', function (Ticket, $rootScope, $compile){

        this.retrieve = {
                all: function(){
                    for(var i=1; i<localStorage.length; ++i){
                       console.log(localStorage[i]);
                    }
                },
                record: function(id){
                    console.log(localStorage[id]);
                }
            };

        this.save = function(){

                if(isNaN(localStorage[0])){
                    localStorage[0] = 1;
                }

                var invoice = Number(localStorage[0]);
                localStorage[invoice] = JSON.stringify(Ticket);
                console.log("localStorage["+invoice+'] : '+localStorage[invoice]);
                localStorage[0] = parseInt(localStorage[0]) + 1;
        };

        this.delete = {
            all: function(){
                localStorage.clear();
                console.log('localStorage cleared');
            },
            record: function(id){
                localStorage.removeItem(id);
                console.log('Record Deleted');
            }
        };
    }])
    .directive('trDropdown', ['Ticket', function(Ticket) {
        return {
            restrict: 'E',
            templateUrl: 'DropdownTemplate.html',
            scope: {
                regarding: '@',
                value: '@option'
            },
            link: function($scope, $elem, $attrs){
                    if($scope.regarding==='Adults'){
                        $elem.attr('required', true);
                    }                       
                    
                    $scope.data = 0;
                    $scope.update = function (){
                        Ticket[$scope.regarding] = $scope.data;
                    };
                }            
        };
    }])
    .controller('DBController', function(){

    })
    .controller('DashboardController', function(){

    });

//	localStorage[0]=JSON.stringify(DBobject, null, '\t');