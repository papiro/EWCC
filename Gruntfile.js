module.exports = function (grunt)
{
	// Project configuration
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		uglify: {
			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
			},
			build: {
				src: 'Project_Files/<%= pkg.name %>.js',
				dest: 'Production/<%= pkg.name %>.min.js'
			}
		},
		jshint: {
			build: ['Gruntfile.js', 'Project_Files/<%= pkg.name %>.js']
		},
		less: {
			build: {
				src: 'Project_Files/<%= pkg.name %>.less',
				dest: 'Project_Files/Production/<%= pkg.name %>.css'
			}
		},
		cssmin: {
			options: {
				banner: '/*<%=grunt.template.today*/'
			},
			build: {
				src: 'Project_Files/<%=pkg.name%>.css',
				dest: 'Production/<%=pkg.name%>.min.css'
			} 
		},
		autoprefix: {
			build: {
				src: 'Production/<%=pkg.name=%>.css',
				dest: 'Production/<%=pkg.name=%>.css'
			}
		},
		watch: {
			options: {
					livereload:9090,
					nospawn: true
				},
			html: {
				files: 'Project_Files/index.html'				
			},
			scripts: {
				files: ['Gruntfile.js', 'Project_Files/<%= pkg.name %>.js'],
				tasks: 'jshint'			
			},
			less: {
				files: 'Project_Files/<%=pkg.name%>.less'
			}
		},
		htmlmin: {
			options: {
				removeComments: true,
				removeCommentsFromCDATA: true,
				collapseWhitespace: true,
				//conservativeCollapse: true,
				//preserveLineBreaks: true,
				collapseBooleanAttributes: true,
				removeAttributeQuotes: true,
				removeRedundantAttributes: true,
				lint: true,
				keepClosingSlash: true,
				minifyJS: true,
				minifyCSS: true
			}			
		}
	});

	//'cssmin' loader
	grunt.loadNpmTasks('grunt-contrib-cssmin');

	//'htmlmin' loader
	grunt.loadNpmTasks('grunt-contrib-htmlmin');

	//'autoprefixer' loader
	grunt.loadNpmTasks('grunt-autoprefixer');

	// Load the plugin that provides the "uglify" task.
	grunt.loadNpmTasks('grunt-contrib-uglify');

	// Jshint loader
	grunt.loadNpmTasks('grunt-contrib-jshint');

	// 'watch' loader
	grunt.loadNpmTasks('grunt-contrib-watch');

	// LESS compiler loader
	grunt.loadNpmTasks('grunt-contrib-less');

	// Define task(s).
	grunt.registerTask('default', 'watch');
	grunt.registerTask('build', ['less', 'autoprefix', 'htmlmin', 'jshint', 'uglify', 'cssmin']);
};